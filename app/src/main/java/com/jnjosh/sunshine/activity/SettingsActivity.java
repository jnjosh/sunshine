package com.jnjosh.sunshine.activity;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

import com.jnjosh.sunshine.R;

public class SettingsActivity extends PreferenceActivity
        implements Preference.OnPreferenceChangeListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.pref_location);
        bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_location_key)));

        addPreferencesFromResource(R.xml.pref_format);
        bindPreferenceSummaryToValue(findPreference(getString(R.string.pref_format_key)));
    }


    private void bindPreferenceSummaryToValue(Preference preference) {
        preference.setOnPreferenceChangeListener(this);

        onPreferenceChange(preference, PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getString(preference.getKey(), ""));
    }


    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String stringValue = newValue.toString();
        if (preference instanceof  ListPreference) {

            ListPreference listPreference = (ListPreference)preference;
            int index = listPreference.findIndexOfValue(stringValue);
            if (index >= 0) {
                preference.setSummary(listPreference.getEntries()[index]);
            }
        }
        else {
            preference.setSummary(stringValue);
        }

        return true;
    }
}
