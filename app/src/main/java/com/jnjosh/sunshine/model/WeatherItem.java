package com.jnjosh.sunshine.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WeatherItem {

    @SerializedName("list")
    public List<ForecastItem> forecastItems;
    public City city;

    public static class City {
        public String name;
    }

    public static class ForecastItem {
        @SerializedName("dt")
        public long dateEpoch;
        public Temperature temp;
        public List<Weather> weather;
    }

    protected static class Temperature {
        public float min;
        public float max;
    }

    protected class Weather {
        @SerializedName("main")
        public String title;
    }

    public static List<String> formattedForecastItems(final WeatherItem weatherItem, final boolean isMetric) {

        List<String> formattedStrings = new ArrayList<>();

        for (ForecastItem forecastItem : weatherItem.forecastItems) {

            String dateTime = getReadableDateString(forecastItem.dateEpoch);
            String description = forecastItem.weather.get(0).title;

            TemperatureUnit highTemperature = new TemperatureUnit(forecastItem.temp.max);
            TemperatureUnit lowTemperature = new TemperatureUnit(forecastItem.temp.min);
            long roundedHigh = Math.round(isMetric ? highTemperature.toCelsius() : highTemperature.toFahrenheit());
            long roundedLow = Math.round(isMetric ? lowTemperature.toCelsius() : lowTemperature.toFahrenheit());

            String formattedHighLow = getTemperatureString(roundedHigh) + " / " + getTemperatureString(roundedLow);

            formattedStrings.add(dateTime + " - " + description + " (" + formattedHighLow + ")");
        }

        return formattedStrings;
    }


    private static String getReadableDateString(long time) {
        Date date = new Date(time * 1000);
        SimpleDateFormat format = new SimpleDateFormat("E, MMM d");
        return format.format(date);
    }


    private static String getTemperatureString(long temperature) {
        return temperature + "º";
    }

}

