package com.jnjosh.sunshine.model;

/**
 * Representation of a Temperature unit.
 * Created by Josh Johnson on 3/20/15.
 */
public final class TemperatureUnit {

    private final float celsiusStore;

    TemperatureUnit(float celsius) {
        celsiusStore = celsius;
    }


    public float toFahrenheit() {
        return celsiusStore * 9f / 5f + 32f;
    }


    public float toCelsius() {
        return celsiusStore;
    }

}
