package com.jnjosh.sunshine.application;

import android.app.Application;

import com.jnjosh.sunshine.BuildConfig;

import timber.log.Timber;

public class SunshineApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
