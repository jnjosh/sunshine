package com.jnjosh.sunshine.api;

import com.jnjosh.sunshine.model.WeatherItem;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

public interface OpenWeatherMapClient {

    @GET("/forecast/daily?mode=json&units=metric")
    void fetchForecast(@Query("q") String postalCode, @Query("cnt") int days, Callback<WeatherItem> callback);

}
