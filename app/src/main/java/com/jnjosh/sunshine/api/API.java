package com.jnjosh.sunshine.api;

import com.google.gson.Gson;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

public class API {

    public static OpenWeatherMapClient getWeatherClient() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://api.openweathermap.org/data/2.5")
                .setConverter(new GsonConverter(new Gson()))
                .build();
        return restAdapter.create(OpenWeatherMapClient.class);
    }

}
