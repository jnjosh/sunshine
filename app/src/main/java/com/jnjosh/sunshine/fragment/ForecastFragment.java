package com.jnjosh.sunshine.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.jnjosh.sunshine.R;
import com.jnjosh.sunshine.activity.DetailActivity;
import com.jnjosh.sunshine.api.API;
import com.jnjosh.sunshine.api.OpenWeatherMapClient;
import com.jnjosh.sunshine.model.WeatherItem;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

public class ForecastFragment extends Fragment {
    @InjectView(R.id.listView_forecast) ListView listView;

    private ArrayAdapter<String> forecastAdapter;
    private OpenWeatherMapClient weatherClient;

    public ForecastFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_my, container, false);
        ButterKnife.inject(this, rootView);

        weatherClient = API.getWeatherClient();
        forecastAdapter = new ArrayAdapter<>(getActivity(),
                R.layout.list_item_forecast,
                R.id.list_item_forecast_textView);

        listView.setAdapter(forecastAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String forecastData = forecastAdapter.getItem(position);
                Intent intent = new Intent(getActivity(), DetailActivity.class);
                intent.putExtra(Intent.EXTRA_TEXT, forecastData);
                startActivity(intent);
            }
        });

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateWeather();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.forecastfragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            updateWeather();
            return true;
        } else if (id == R.id.action_viewLocation) {
            String postalCode = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getString(R.string.pref_location_key), "27613");
            Uri uri = Uri.parse("geo:0,0?q=" + postalCode);

            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
            else {
                Timber.e("Can not load a map intent");
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void updateWeather() {
        String postalCode = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getString(R.string.pref_location_key), "27613");
        postalCode = postalCode + ",USA";

        weatherClient.fetchForecast(postalCode, 5, new Callback<WeatherItem>() {
            @Override
            public void success(WeatherItem weatherItem, Response response) {
                if (weatherItem != null) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    int index = Integer.parseInt(preferences.getString(getString(R.string.pref_format_key), "0"));

                    forecastAdapter.clear();
                    forecastAdapter.addAll(WeatherItem.formattedForecastItems(weatherItem, index != 0));

                    Toast.makeText(getActivity(), weatherItem.city.name, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getActivity(), "No weather data found. Sorry.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Timber.e(error.getLocalizedMessage());
            }
        });
    }

}
